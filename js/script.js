//Теория
// try...catch позволяет проверять блок кода на наличие ошибок.
// Например, если мы пишем инструкцию, которая бы при загрузке страницы выводила модальное окно с сообщением о приветствии, и делаем опечатку в команде: alert ==> aalert, то у нас ничего не произойдёт. Либо, как в примере ниже, когда имеется массив объектов, и необходимо вывести на экран только те объекты, в которых имеются все свойства, а для объектов, где нет каких-либо свойств, генерировать ошибку.
// Таким образом, используя try...catch, мы можем отлавливать, генерировать и выводить сообщение об ошибке.

//Задание
const books = [
    {
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70
    },
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    },
    {
      name: "Тысячекратная мысль",
      price: 70
    },
    {
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    },
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    },
];

const root = document.querySelector('.root');
const ul = document.createElement('ul');
root.append(ul);

for (const key in books) {
  const {author, name, price} = books[key];
  let li = document.createElement('li');
  li.textContent = `Автор: ${author}, --- \r\n Название: ${name}, --- Стоимость: ${price}`;
  try {
    if(author === undefined || name === undefined || price === undefined) {
      if(!Object.keys(books[key]).includes('author')) {
        throw new Error('Author property is not defined in ' + key + ' object');
      }
      else if(!Object.keys(books[key]).includes('name')) {
        throw new Error('Name property is not defined in ' + key + ' object');
      }
      else {
        throw new Error('Price property is not defined in ' + key + ' object');
      }
    }
    ul.append(li);
  } catch (e) {
    console.log(e);
  }
}
